import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton gyozaButton;
    private JButton yakisobaButton;
    private JTextPane list;
    private JTextPane total;
    private JButton checkOutButton;

    int sum_price=0;
    int sum(String food) {
        if (food == "Tempura") {
            sum_price += 100;
        } else if (food == "Karaage") {
            sum_price += 100;
        } else if (food == "Gyoza") {
            sum_price += 100;
        } else if (food == "Udon") {
            sum_price += 100;
        } else if (food == "Yakisoba") {
            sum_price += 100;
        } else if (food == "Ramen") {
            sum_price += 100;
        }
        return sum_price;
    }

    void order(String food) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            String f = list.getText();
            list.setText(f + "\n" + food);
            String str_sum = String.valueOf(sum(food));  //int型からString型に変換
            total.setText(str_sum);

            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible.");
        }
    }

    void Checkout(String checkout) {
        int checkout_confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to checkout?",
                "Checkout Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (checkout_confirmation == 0) {
            list.setText("");  //getTextを使わないときは値が新しく書き換えられることを利用
            total.setText("0");  //空白にした場合、チェックアウトすると何も表示されなくなるので0を入れる
            sum_price=0;  //チェックアウトした後に合計代金を0にリセットする

            JOptionPane.showMessageDialog(null,
                    "Thank you for coming!");
        }
    }

    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
                }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Checkout("");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
